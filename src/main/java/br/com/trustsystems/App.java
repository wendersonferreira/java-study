package br.com.trustsystems;

import java.util.Scanner;

public class App {

    private static int sum(int x, int y) {
        return x + y;
    }

    public static void main(String[] args) {
        int x, y, z;

        System.out.println("Enter two integers to calculate their sum");
        Scanner in = new Scanner(System.in);

        x = in.nextInt();
        y = in.nextInt();
        z = sum(x, y);

        System.out.println("Sum of the integers = " + z);
    }
}
